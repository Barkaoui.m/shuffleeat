//
//  MainTabBarController.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 22/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {

    let main = MainCoordinator(navigationController: UINavigationController())
    let buy = BuyCoordinator(navigationController: UINavigationController(), selectedProduct: 0)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        main.start()
        buy.start()
        viewControllers = [main.navigationController, buy.navigationController]
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
