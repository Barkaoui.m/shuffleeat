//
//  Buying.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 22/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import Foundation

protocol Buying: AnyObject {
    func buySubscription(to productType: Int) 
}
