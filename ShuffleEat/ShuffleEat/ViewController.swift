//
//  ViewController.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 21/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,Storyboarded {

     var coordinator: MainCoordinator?
    @IBOutlet weak var product: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buyTapped(_ sender: Any) {
        coordinator?.buySubscription(to: product.selectedSegmentIndex)
    }

    @IBAction func createAccount(_ sender: Any) {
        coordinator?.accountCreate()
    }

}

