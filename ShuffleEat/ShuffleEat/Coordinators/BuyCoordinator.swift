//
//  buyCoordinator.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 22/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import Foundation
import UIKit

class BuyCoordinator: Coordinator {
    
    var parentCoordinator: MainCoordinator?
    var childCoordinators: [Coordinator] = [Coordinator]()
    var navigationController: UINavigationController
    var selectedProduct: Int
    init(navigationController: UINavigationController, selectedProduct: Int = 0 ) {
        self.navigationController = navigationController
        self.selectedProduct = selectedProduct
    }
    
    func start() {
        let vc = BuyViewController.instantiate()
        vc.coordinator = self
        vc.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 1)
        vc.selectedProduct = self.selectedProduct
        navigationController.pushViewController(vc, animated: false)
      }
    
//    func didFinishBuying() {
//        parentCoordinator?.childDidFinish(self)
//    }
}
