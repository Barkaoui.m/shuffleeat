//
//  MainCoordinator.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 21/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import UIKit

class MainCoordinator: NSObject, Coordinator, UINavigationControllerDelegate {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var vController:ViewController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        navigationController.delegate = self
        vController = ViewController.instantiate()
        vController?.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0 )
        vController?.coordinator = self
        self.navigationController.pushViewController(vController ?? UIViewController() , animated: false)
    }

    func childDidFinish( _ child : Coordinator?) {
        for (index, coordinator) in self.childCoordinators.enumerated() {
            if coordinator === child {
                self.childCoordinators.remove(at: index)
            break
        }
    }
}
    
    func navigationController( _ navigationController: UINavigationController, didShow viewController:UIViewController, animated: Bool) {
        
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }
        
        if let buyViewController = fromViewController as? BuyViewController {
            childDidFinish(buyViewController.coordinator)
        }
    }
    
    func buySubscription(to productType: Int) {
           let child = BuyCoordinator(navigationController: self.navigationController, selectedProduct: productType)
               child.parentCoordinator = self
               child.start()
               self.childCoordinators.append(child)
               
     }
     
    
}


extension MainCoordinator: AccountCreating {
    func accountCreate() {
         let vc = CreateAccountViewController.instantiate()
               vc.coordinator = self
               navigationController.pushViewController(vc, animated: true)
           
    }
    
    
}


