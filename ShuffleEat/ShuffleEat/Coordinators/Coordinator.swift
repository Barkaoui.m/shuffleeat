//
//  Coordinator.swift
//  coordinatorStarter
//
//  Created by Malek BARKAOUI on 21/07/2020.
//  Copyright © 2020 Malek BARKAOUI. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator : AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
